generator client {
  provider = "prisma-client-js"
}

datasource db {
  provider = "postgresql"
  url      = env("DATABASE_URL")
}

model User {
    id String @id @default(cuid())
    createdAt DateTime @default(now()) @map("created_at")
    updatedAt DateTime @updatedAt @map("updated_at")

    email String @unique
    name String?
    password String

    tasks Task[]
    timeBlocks TimeBlock[]
    pomodoroSessions PomodoroSession[]

    userSettings UserSettings?
}

model UserSettings {
    id String @id @default(cuid())

    workInterval Int @default(50) @map("work_interval")
    breakInterval Int @default(10) @map("break_interval")
    intervalsCount Int @default(7) @map("intervals_count")

    user User? @relation(fields: [userId], references: [id])
    userId String @unique @map("user_id")
}

model Task {
    id String @id @default(cuid())
    createdAt DateTime? @default(now()) @map("created_at")
    updatedAt DateTime @updatedAt @map("updated_at")

    name String
    priority Priority?
    isCompleted Boolean? @default(false) @map("is_completed")

    user User @relation(fields: [userId], references: [id])
    userId String @map("user_id")
}

model TimeBlock {
    id String @id @default(cuid())
    createdAt DateTime @default(now()) @map("created_at")
    updatedAt DateTime @updatedAt @map("updated_at")

    name String
    color String?
    duration Int
    order Int @default(1)

    user User @relation(fields: [userId], references: [id])
    userId String @map("user_id")
}

model PomodoroSession {
    id String @id @default(cuid())
    createdAt DateTime @default(now()) @map("created_at")
    updatedAt DateTime @updatedAt @map("updated_at")

    isCompleted Boolean? @default(false) @map("is_completed")

    user User @relation(fields: [userId], references: [id])
    userId String @map("user_id")

    rounds PomodoroRound[]
}

model PomodoroRound {
    id String @id @default(cuid())
    createdAt DateTime @default(now()) @map("created_at")
    updatedAt DateTime @updatedAt @map("updated_at")

    isCompleted Boolean? @default(false) @map("is_completed")
    totalSeconds Int @map("total_seconds")

    pomodoroSession PomodoroSession @relation(fields: [pomodoroSessionId], references: [id], onDelete: Cascade)
    pomodoroSessionId String @map("pomodoro_session_id")
}

enum Priority {
    low
    medium
    high
}