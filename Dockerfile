FROM node:20.14.0-alpine AS builder

WORKDIR /usr/src/app

COPY package.json ./

RUN npm install

COPY . .

RUN npx prisma generate  
RUN npm run build