import { IsString, IsOptional, IsNumber, Min } from 'class-validator';

export class TimeBlockDto {
	@IsString()
	name: string;

	@IsOptional()
	@IsString()
	color?: string;

	@IsNumber()
	@Min(1)
	duration: number;

	@IsOptional()
	@IsNumber()
	order: number;
}
