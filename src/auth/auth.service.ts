import {
	BadRequestException,
	Injectable,
	NotFoundException,
	UnauthorizedException
} from '@nestjs/common';
import { JwtService } from '@nestjs/jwt';
import { Response } from 'express';
import { verify } from 'argon2';

import { UserService } from 'src/user/user.service';

import type { AuthDto } from './dto';

import {
	JWT_ACCESS_LIFETIME,
	JWT_REFRESH_LIFETIME,
	DOMAIN_URL,
	IS_PROD_MODE
} from 'src/const/env';

@Injectable()
export class AuthService {
	EXPIRES_DAY_REFRESH_TOKEN = 1;
	REFRESH_TOKEN_NAME = 'refreshToken';

	constructor(
		private jwt: JwtService,
		private userService: UserService
	) {}

	async login(dto: AuthDto) {
		const { password, ...user } = await this.validateUser(dto);
		const tokens = this.issueTokens(user.id);
		return {
			user,
			...tokens
		};
	}

	async register(dto: AuthDto) {
		const existingUser = await this.userService.getByEmail(dto.email);

		if (existingUser) throw new BadRequestException('User already exists');

		const { password, ...user } = await this.userService.create(dto);

		const tokens = this.issueTokens(user.id);
		return {
			user,
			...tokens
		};
	}

	async getNewTokens(refreshToken: string) {
		const result = await this.jwt.verifyAsync(refreshToken);
		if (!result) throw new UnauthorizedException('Invalid refresh token');

		const { password, ...user } = await this.userService.getById(result.id);

		const tokens = this.issueTokens(user.id);
		return {
			user,
			...tokens
		};
	}

	private issueTokens(userId: string) {
		const data = { id: userId };

		const accessToken = this.jwt.sign(data, { expiresIn: JWT_ACCESS_LIFETIME });

		const refreshToken = this.jwt.sign(data, {
			expiresIn: JWT_REFRESH_LIFETIME
		});

		return { accessToken, refreshToken };
	}

	private async validateUser(dto: AuthDto) {
		const user = await this.userService.getByEmail(dto.email);

		if (!user) throw new NotFoundException('User not found');

		const isValid = await verify(user.password, dto.password);

		if (!isValid) throw new UnauthorizedException('Invalid password');

		return user;
	}

	addRefreshTokenToResponse(res: Response, refreshToken: string) {
		const expiresIn = new Date();

		expiresIn.setDate(expiresIn.getDate() + this.EXPIRES_DAY_REFRESH_TOKEN);

		res.cookie(this.REFRESH_TOKEN_NAME, refreshToken, {
			httpOnly: true,
			domain: DOMAIN_URL,
			expires: expiresIn,
			secure: IS_PROD_MODE,
			sameSite: IS_PROD_MODE ? 'lax' : 'none'
		});
	}

	removeRefreshTokenFromResponse(res: Response) {
		res.cookie(this.REFRESH_TOKEN_NAME, '', {
			httpOnly: true,
			domain: DOMAIN_URL,
			expires: new Date(0),
			secure: IS_PROD_MODE,
			sameSite: IS_PROD_MODE ? 'lax' : 'none'
		});
	}
}
