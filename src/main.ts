import { NestFactory } from '@nestjs/core';
import { AppModule } from './app.module';
import * as cookieParser from 'cookie-parser';

import * as dotenv from 'dotenv';

dotenv.config();

import { ALLOWED_HOSTS, HOST_PORT, BASE_URL } from './const/env';

async function bootstrap() {
	const app = await NestFactory.create(AppModule);

	app.setGlobalPrefix(`${BASE_URL}/api`);
	app.use(cookieParser());
	app.enableCors({
		origin: ALLOWED_HOSTS.split(' '),
		credentials: true,
		exposedHeaders: 'set-cookie'
	});

	await app.listen(HOST_PORT);
}
bootstrap();
