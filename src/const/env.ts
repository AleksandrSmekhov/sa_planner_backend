/* APP */
export const { HOST_PORT, ALLOWED_HOSTS, DOMAIN_URL, BASE_URL } = process.env;

/* JWT */
export const { JWT_SECRET, JWT_ACCESS_LIFETIME, JWT_REFRESH_LIFETIME } =
	process.env;

/* DEV OR PROD MODE */
const { MODE } = process.env;
export const IS_PROD_MODE = MODE === 'PROD';
