import { Module } from '@nestjs/common';

import { TaskModule } from 'src/task/task.module';
import { PrismaService } from 'src/prisma.service';

import { UserService } from './user.service';
import { UserController } from './user.controller';

@Module({
	imports: [TaskModule],
	controllers: [UserController],
	providers: [UserService, PrismaService],
	exports: [UserService]
})
export class UserModule {}
