import { Injectable } from '@nestjs/common';
import { hash } from 'argon2';

import { PrismaService } from 'src/prisma.service';
import { TaskService } from 'src/task/task.service';

import type { AuthDto } from 'src/auth/dto';
import type { UserDto } from './dto';

@Injectable()
export class UserService {
	constructor(
		private prisma: PrismaService,
		private taskService: TaskService
	) {}

	getById(id: string) {
		return this.prisma.user.findUnique({
			where: {
				id
			},
			include: {
				tasks: true,
				userSettings: true
			}
		});
	}

	getByEmail(email: string) {
		return this.prisma.user.findUnique({
			where: { email },
			include: {
				userSettings: true
			}
		});
	}

	getIntervalsCount(userId: string) {
		return this.prisma.userSettings.findUnique({
			where: { userId },
			select: {
				intervalsCount: true
			}
		});
	}

	async getProfile(id: string) {
		const profile = await this.getById(id);

		const totalTasks = profile.tasks.length;
		const completedTasks = await this.taskService.getCompletedTasksAmount(id);

		const todayEnd = new Date()
		todayEnd.setHours(23, 59, 59, 999);

		const weekEnd = new Date();
		weekEnd.setHours(23, 59, 59, 999);
		weekEnd.setDate(weekEnd.getDate() + 7);

		const todayTasks = await this.taskService.getDateTasksAmount(
			id,
			todayEnd
		);

		const weekTasks = await this.taskService.getDateTasksAmount(id, weekEnd);

		const { password, ...user } = profile;

		return {
			user,
			statistics: { totalTasks, completedTasks, todayTasks, weekTasks }
		};
	}

	async create(dto: AuthDto) {
		const user = {
			email: dto.email,
			name: '',
			password: await hash(dto.password)
		};

		return await this.prisma.user.create({
			data: {
				...user,
				userSettings: {
					create: {}
				}
			}
		});
	}

	async update(id: string, { settings, ...dto }: UserDto) {
		const data = dto.password
			? { ...dto, password: await hash(dto.password) }
			: dto;

		if (settings) {
			await this.prisma.userSettings.upsert({
				where: { userId: id },
				update: { userId: id, ...settings },
				create: { userId: id, ...settings }
			});
		}

		return this.prisma.user.update({
			where: { id },
			data,
			select: { id: true, name: true }
		});
	}
}
