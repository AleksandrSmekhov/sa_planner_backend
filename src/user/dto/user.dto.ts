import {
	IsEmail,
	IsString,
	MinLength,
	IsOptional,
	IsNumber,
	Min,
	Max,
	ValidateNested
} from 'class-validator';
import { Type } from 'class-transformer';

class UserSettingsDto {
	@IsOptional()
	@IsNumber()
	@Min(1)
	workInterval?: number;

	@IsOptional()
	@IsNumber()
	@Min(1)
	breakInterval?: number;

	@IsOptional()
	@IsNumber()
	@Min(1)
	@Max(10)
	intervalsCount?: number;
}

export class UserDto {
	@IsOptional()
	@IsEmail()
	email?: string;

	@IsOptional()
	@IsString()
	name?: string;

	@IsOptional()
	@IsString()
	@MinLength(6, {
		message: 'password must be at least 6 characters long'
	})
	password?: string;

	@IsOptional()
	@ValidateNested()
	@Type(() => UserSettingsDto)
	settings?: UserSettingsDto;
}
