import { Injectable } from '@nestjs/common';

import { PrismaService } from 'src/prisma.service';
import { TaskDto } from './dto';

@Injectable()
export class TaskService {
	constructor(private prisma: PrismaService) {}

	getCompletedTasksAmount(id: string) {
		return this.prisma.task.count({
			where: {
				userId: id,
				isCompleted: true
			}
		});
	}

	getDateTasksAmount(id: string, endDate: Date) {
		const todayStart = new Date();
		todayStart.setHours(0, 0, 0, 0);

		return this.prisma.task.count({
			where: {
				userId: id,
				createdAt: {
					gte: todayStart.toISOString(),
					lte: endDate.toISOString()
				}
			}
		});
	}

	async getAll(userId: string) {
		return this.prisma.task.findMany({
			where: {
				userId
			}
		});
	}

	async create(dto: TaskDto, userId: string) {
		return this.prisma.task.create({
			data: {
				...dto,
				user: {
					connect: {
						id: userId
					}
				}
			}
		});
	}

	async update(dto: Partial<TaskDto>, taskId: string, userId: string) {
		return this.prisma.task.update({
			where: {
				userId,
				id: taskId
			},
			data: Object.keys(dto).includes('createdAt')
				? {
						...dto,
						createdAt: dto.createdAt || null
					}
				: dto
		});
	}

	async delete(taskId: string) {
		return this.prisma.task.delete({
			where: {
				id: taskId
			}
		});
	}
}
