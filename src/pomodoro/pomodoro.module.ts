import { Module } from '@nestjs/common';

import { UserModule } from 'src/user/user.module';
import { PrismaService } from 'src/prisma.service';

import { PomodoroService } from './pomodoro.service';
import { PomodoroController } from './pomodoro.controller';

@Module({
	imports: [UserModule],
	controllers: [PomodoroController],
	providers: [PomodoroService, PrismaService]
})
export class PomodoroModule {}
