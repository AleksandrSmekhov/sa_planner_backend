import {
	Controller,
	Get,
	HttpCode,
	Put,
	Post,
	Delete,
	UsePipes,
	ValidationPipe,
	Param,
	Body
} from '@nestjs/common';

import { Auth, CurrentUser } from 'src/auth/decorators';

import { PomodoroService } from './pomodoro.service';
import { PomodoroRoundDto, PomodoroSessionDto } from './dto';

@Controller('user/timer')
export class PomodoroController {
	constructor(private readonly pomodoroService: PomodoroService) {}

	@Get('today')
	@Auth()
	async getTodaySession(@CurrentUser('id') userId: string) {
		return this.pomodoroService.getTodaySession(userId);
	}

	@HttpCode(200)
	@Post()
	@Auth()
	async create(@CurrentUser('id') userId: string) {
		return this.pomodoroService.create(userId);
	}

	@UsePipes(new ValidationPipe())
	@HttpCode(200)
	@Put('round/:id')
	@Auth()
	async updateRound(@Param('id') id: string, @Body() dto: PomodoroRoundDto) {
		return this.pomodoroService.updateRound(dto, id);
	}

	@UsePipes(new ValidationPipe())
	@HttpCode(200)
	@Put(':id')
	@Auth()
	async update(
		@Param('id') id: string,
		@Body() dto: PomodoroSessionDto,
		@CurrentUser('id') userId: string
	) {
		return this.pomodoroService.updateSession(dto, id, userId);
	}

	@HttpCode(200)
	@Delete(':id')
	@Auth()
	async deleteSession(
		@Param('id') id: string,
		@CurrentUser('id') userId: string
	) {
		return this.pomodoroService.deleteSession(id, userId);
	}
}
